package;

import com.gelert.memfps.MemFPS;
import openfl.display.Sprite;

/**
 * ...
 * @author gelert
 */
class Main extends Sprite {

	public function new() {
		super();
		var meter = new MemFPS();
		meter.oneLine = true;
		addChild(meter);
	}
}
