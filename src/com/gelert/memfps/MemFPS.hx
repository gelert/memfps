package com.gelert.memfps;

import haxe.Timer;
import openfl.display.DisplayObject;
import openfl.events.Event;
import openfl.system.System;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;

/**
 * ...
 * @author gelert
 */
class MemFPS extends TextField {
	
	static var _FREQUENCY = 0.5;	//time period between calculations;
	static var _MB = 1024 * 1024;
	static var _ACCURACY = 1000;
	static var _multiplier = _ACCURACY / _MB;

	public function new(enterFrameDispatcher:DisplayObject = null) {
		super();
		var dispatcher = enterFrameDispatcher == null ? this : enterFrameDispatcher;
		selectable = false;
		mouseEnabled = false;
		autoSize = TextFieldAutoSize.LEFT;
		dispatcher.addEventListener(Event.ENTER_FRAME, handler_enterFrame);
	}
	
	var _stamp = Timer.stamp();
	var _frameCounter:Int = 0;
	var _memPeak:Float = 0;
	
	public var oneLine = false;
	
	function handler_enterFrame(event:Event) {
		_frameCounter++;
		var mem = Math.round(System.totalMemory * _multiplier) / _ACCURACY;
		if(_memPeak < mem) _memPeak = mem;
		var stamp = Timer.stamp();
		var delta = stamp - _stamp;
		if(delta < _FREQUENCY) return;
		var fps = Math.round(_frameCounter / delta);
		var delimiter = oneLine ? ', ' : '\n';
		text = 'fps: ' + fps + delimiter + 'memPeak: ' + _memPeak + delimiter + 'mem: ' + mem;
		_stamp = stamp;
		_frameCounter = 0;
	}
}